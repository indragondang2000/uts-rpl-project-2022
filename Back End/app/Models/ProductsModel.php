<?php

namespace App\Models;

use CodeIgniter\Model;

class ProductsModel extends Model
{
    protected $table      = 'products';
    protected $primaryKey = 'id_product';
    protected $useAutoIncrement = true;

    protected $allowedFields = ['name','image', 'deskripsi', 'price', 'stock'];
}