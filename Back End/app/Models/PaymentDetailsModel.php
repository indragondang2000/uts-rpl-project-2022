<?php

namespace App\Models;

use CodeIgniter\Model;

class PaymentDetailsModel extends Model
{
    protected $table      = 'paymentdetails';
    protected $primaryKey = 'id_paymentdetail';
    protected $useAutoIncrement = true;

    protected $allowedFields = ['id_order', 'date', 'payment_methode', 'payment_bill', 'status', 'payment_proof'];
}